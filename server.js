const express = require('express')
const http = require('http')
const socketIO = require('socket.io')

// our localhost port
const port = 4001

const app = express()

// our server instance
const server = http.createServer(app)

// This creates our socket using the instance of the server
const io = socketIO(server)

// This is what the socket.io syntax is like, we will work this later
const nsp = io.of('/receive-data');
let isMasterExist = false;
let pseudoList = [];
let hasGameStart = false;
nsp.on('connection', function(socket){
    let pseudo;
    let isMaster=false;

    socket.on('masterConnected', (msg)=>{
        if(isMasterExist === true){
            socket.emit('canBeMaster',false);
        }
        else{
            isMasterExist=msg;
            isMaster=msg;
            socket.emit('isMaster',true);
            console.log("Game master is now connected, user can play");
        }
        
    })
    socket.on('newPlayer', function(msg){
        if(hasGameStart){
            socket.emit("noRoom","Wait until the end of the game")
        }
        else{
            const index = pseudoList.indexOf(msg);
            const pseudoNotExist = (index === -1);
            if(isMasterExist && pseudoNotExist){
                console.log(msg+" join the game");
                socket.emit('connectionMaster',true)
                socket.broadcast.emit("addPlayer",msg);
                pseudo=msg;
                pseudoList.push(msg)
            }
            else if(!isMasterExist){
                socket.emit("noRoom","You need a game master")
            }
            else if(!pseudoNotExist){
                socket.emit("noRoom","Le pseudo est utilisé par un autre joueur")
            }
        }
      });
    
    socket.on("startTheGame", msg => {
        hasGameStart=msg;
        socket.broadcast.emit('enableBuzz',true)
    });

    socket.on("resetBuzz", msg =>{
        socket.broadcast.emit('enableBuzz',true)
    })
    socket.on("endTheGame", msg => {
        hasGameStart=msg
        socket.broadcast.emit("disableBuzz",true);
    });

    socket.on("quitPlayer", (msg) => {
        socket.broadcast.emit('deletePlayer',pseudo);
        const index = pseudoList.indexOf(pseudo);
        pseudoList.splice(index, 1);
    });


    socket.on("buzz", (msg)=> {
        socket.emit('disableBuzz',true);
        socket.broadcast.emit("disableBuzz",true);
        socket.broadcast.emit('showScore',pseudo);
    })

    socket.on('response', (msg)=>{
        
        if(msg.value){
            socket.broadcast.emit('result',{dest:msg.pseudo, message:"Vous avez bien répondu", answer: true});
            socket.broadcast.emit('enableBuzz',true)         
        }
        else{
            socket.broadcast.emit('result',{dest:msg.pseudo, message:"Vous avez mal répondu", answer: false})
        }
    });

    socket.on('disconnect', (msg) => {
        if(isMaster){
            isMasterExist = false;
            hasGameStart = false;
            socket.broadcast.emit('changeMaster',false);
            if(pseudoList.length > 0){
                console.log("all user are disconneted");
                pseudoList = [];
            }
            console.log('The game master logged out');
            
        }
        else if (pseudo !== undefined){
            console.log(pseudo + " logged out");
            socket.broadcast.emit('deletePlayer',pseudo);
            const index = pseudoList.indexOf(pseudo);
            pseudoList.splice(index, 1);
        }
    });
    
});

server.listen(port, () => console.log(`Listening on port ${port}`))